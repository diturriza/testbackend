/**
 * ClothesController
 *
 * @description :: Server-side logic for managing clothes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
	'index' : function (req,res){
		res.view();
	},

	'new' : function(req,res){
		res.view();
	},

	'create' : function(req,res,next){
		Clothes.create(req.params.all(),function clothesCreated (err,clothes){
			if(err) {
				res.serverError(err);
			}
			if(!clothes){
				res.ok({
					status: "failed",
					data: []
				});
			}else {
				res.ok({
					status: "success",
					data: clothes
				});
			}
			res.redirect('/clothes/new');
		});
	},

	find: function(req,res){

		var searchAll = req.param('termSearch');

		// console.log(searchAll);

		var search_terms = searchAll.toString().split(' ');

		var resultSearch = [];

		var length_search = search_terms.length;

		for(i = 0 ; i< length_search ; i++)
		{
			var newObj = {
				word: search_terms[i],
				style_text : ""
			}
			resultSearch.push(newObj);
		}

		SearchService.getStylesForBrandsClothes(resultSearch, function(err, result){
	      if (err)
	        return res.view({ result : [] });
	    
	      return res.view({result: result.result});
	    });
	}

};

