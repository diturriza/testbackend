/**
* Clothes.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports  = {

  // Attributes are basic pieces of information about a model
  attributes: {
    brand:{
  		type:'string'
  	},
  	clothes_type:{
  		type:'string'
  	}
  }
};
