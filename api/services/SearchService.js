/**
 * Created by Daniel Iturriza on 2015/01/30
 */

module.exports = {
  getStylesForBrandsClothes : getStyles

}



function getStyles(search, cb){

	async.waterfall([
			function loadData (callback){
				Clothes.find({},function loader(err,clothes){
					if(err) callback(err);

					callback(null, clothes);
				})
			},
			function findBrands(clothes, callback){
				clothes.forEach(function (item){
					var brand = item.brand;
					if(brand != "")
					{
						search.forEach(function (element){
							var word = element.word;
							var result = brand.match(new RegExp(word,'i'));
							if(result && element.style_text == "")
							{
								element.style_text = 'bold';
							}
						});	
					}			
				});
				callback(null, clothes);
			},
			function findTypes(clothes, callback){
				clothes.forEach(function (item){
					var clothes_type = item.clothes_type;
					if(clothes_type != "")
					{
						search.forEach(function (element){
							var word = element.word;
							var result = clothes_type.match(new RegExp(word,'i'));
							if(result && element.style_text == "")
							{
								element.style_text = 'italic';
							}
								
						});	
					}			
				});
				callback(null, search);
			}], 
			function (error,result){
				if(error)
					cb({err:err});
				if(result){
					// console.log(result);
					cb(null, {
						result: result
					})
				}
			});
}
