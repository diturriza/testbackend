# testBackend

a [Sails](http://sailsjs.org) application.

## Estructura del Proyecto
* Se utilizó Sails Js como Framework de desarrollo.
* Se creó un modelo: Clothes.
* MongoDb para la creación de la colección: "storecompany".
* Para el maquetado de las vistas se utilizo el framework Bootstrap 3.

## Instalación y corrida del Proyecto
Dentro de su terminal:

1. git clone este repositorio
2. testBackend/npm install
3. testBackend/mongorestore dump/
4. testBackend/sails lift

Ingresar a http://localhost:1337/.

## Funcionamiento del Proyecto
Al ingresar a la dirección del proyecto se mostrará una vista, dicha vista proporciona un input tipo texto donde se podrá indicar la búsqueda y una vez listo se deberá dar click en 'Search'. Acto seguido, se mostrará en una nueva vista el resultado de la búsqueda, cumpliendo con las actividades, notas y premisas descritas en el problema.

